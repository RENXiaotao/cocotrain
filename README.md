I count the points of connecting trains for the board game, Ticket to Ride.  At first, it is better to count the points straightforward.

Steps:
1. Read a ref and a game photo
2. Find game board and transform 
3. Find cities and rails on ref
4. Find cities and rails on game board
5. Read legends
6. Count rails for each color and calculate points
7. Find longest rail and grade



- [x] Interactively get board
- [x] Perspectively transform to a rect
- [ ] Detect cities and rails in ref
- [ ] Detect rails in game
- [ ] Detect legends
- [ ] Detect rails built
- [ ] Calculate and mark the points
- [ ] Find the longest rail
- [ ] Automatically get board
- [ ] NN understands the train
- [ ] NN understands the board

Optional
- [ ] Magnifying glass over cursor hover for interactive board corner selection
- [ ] Tip a rail to highlight scoreboard
- [ ] Redraw cities and rails
