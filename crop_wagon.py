import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

import argparse
import os

def crop(im, im_dir, dst_dir):
    img = cv.imread(f"{im_dir}{im}")
    # TODO : check again.
    gray= cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    th, threshed = cv.threshold(gray, 130, 255, cv.THRESH_BINARY_INV)
    ## Morph-op to remove noise
    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (11,11))
    morphed = cv.morphologyEx(threshed, cv.MORPH_CLOSE, kernel)

    ## Find the max-area contour
    cnts = cv.findContours(morphed, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)[-2]
    cnt = sorted(cnts, key=cv.contourArea)[-1]
    x,y,w,h = cv.boundingRect(cnt)
    # print(x,y,w,h)
    offset = 0
    #TODO: calc to find the best offset, or 4 offsets
    # or offset = 0
    dst = img[(y-offset):y+h+offset, (x-offset):x+w+offset]
    cv.imwrite(f"{dst_dir}{im}", dst)
def imgplot(img):
    # plt.subplot()
    pass 

if __name__ == "__main__":
    # TODO: complete ap 
    # No need, the fun will finally be called by another
    # But check is necessary
    #ap = argparse.ArgumentParser()
    #group = ap.add_mutually_exclusive_group(required=True)
    #group.add_argument("--images", help="Directory of images to be scanned")
    #group.add_argument("--image", help="Path to single image to be scanned")
    im_dir = "/Users/xiaotao/Downloads/ZugCounter/wagons/" 
    valid_formats = [".jpg", ".jpeg", ".jp2", ".png", ".bmp", ".tiff", ".tif"]
    get_ext = lambda f: os.path.splitext(f)[1].lower()
    im_files = [f for f in os.listdir(im_dir) if get_ext(f) in valid_formats]
    # TODO : check len(im_files), prompt if zero
    #dst_dir = "/Users/xiaotao/Dropbox/Python/cocotrain/wagons_cd/" # cd = cropped, 
    dst_dir = "wagons_cd/"
    # FIXME: relative or absolute path
    # TODO : if dst_dir not exists, create it
    [crop(im, im_dir, dst_dir) for im in im_files] #Pythonic

